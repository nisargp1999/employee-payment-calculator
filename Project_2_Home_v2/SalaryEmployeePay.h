#ifndef SalaryEmployeePay_H
#define SalaryEmployeePay_H

#include "Employee.h"

using namespace std;

class SalaryEmployeePay : public Employee
{
public:
	SalaryEmployeePay();
	double getAnnualPay();
	double getWeeklyPay();
	int getTaxRate();
	void printAnnualPay();
	void printWeeklyPay();
	void setAnnualPay(double thisAnnualPay);
	void setTaxRate(int thisTaxRate);
	void printTaxRate();
private:
	void calcWeeklyPay();

	/*Employee thisEmployee;*/
	double annualPay;
	double weeklyPay;
	int taxRate;
};

#endif
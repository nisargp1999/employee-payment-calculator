
#include "stdafx.h"
#include <fstream>
#include "SalaryEmployeePay.h"
#include "HourlyEmployeePay.h"
#include "AgencyEmployeePay.h"

using namespace std;


bool checkIfNum(string);
bool checkId(string);
bool checkSSN(string);
bool checkWorkStatus(char);

int main()
{
	ofstream output;
	output.open("pay.dat");
	string firstName, lastName, SSN, employeeId, companyName;
	double totalNumHours, weeklyPay, hourlyPayRate, annualPay;
	int userInput, taxRate;
	char workStatus;

	do {
		cout << "Type 1 for Salary Employee\nType 2 for Hourly Employee\nType 0 to quit:" << endl;
		cin >> userInput;

		if (userInput == 2) {
			system("CLS");
			HourlyEmployeePay myEmployee;


			cout << "Your full name: ";
			cin >> firstName;
			cin >> lastName;
			myEmployee.setFirstName(firstName);
			myEmployee.setLastName(lastName);


			//.................................


			do {
				cout << endl << "Social Security #: ";
				cin >> SSN;
				if (checkIfNum(SSN) == false)
				{
					cout << endl << "Sorry, Social security number is not in right format.\nTry again:";
				}
			} while (checkIfNum(SSN) == false);
			myEmployee.setSSN(SSN);
			cout << endl << "SSN successfully taken";


			//.................................


			do {
				cout << endl << "Employee Id: ";
				cin >> employeeId;
				if (checkId(employeeId) == false)
				{
					cout << endl << "Sorry, Id is not in right format. Try again:";
				}
			} while (checkId(employeeId) == false);
			myEmployee.setEmployeeId(employeeId);
			cout << endl << "Employee # successfully taken";

			//.................................


			do {
				do {
					cout << endl << "Number of hours worked in a week:";
					cin >> totalNumHours;
					if (totalNumHours >= 60 || totalNumHours < 0)
					{
						cout << "Sorry, number of hours worked in a week can only be positive and less than 60.\nPlease try again." << endl;
					}
					cout << endl << "# of hours worked successfully taken";
				} while (totalNumHours >= 60 || totalNumHours < 0);
				cout << endl << "Hourly Pay Rate:";
				cin >> hourlyPayRate;
				myEmployee.setHourlyPayRate(hourlyPayRate);
				myEmployee.setTotalNumHours(totalNumHours);
				if (myEmployee.getHourlyPayRate() < 10)
				{
					cout << endl << "Sorry, hourly pay rate cannot be less than 10.\nPlease try again:" << endl;
				}
				else if (myEmployee.getHourlyPayRate() > 75) {
					cout << endl << "Sorry, hourly pay rate cannot be more than 75.\nPlease try again:" << endl;
				}
				else if (totalNumHours > 60)
				{
					cout << endl << "Sorry, total number of hours cannot by more than 60" << endl;
				}
			} while (myEmployee.getHourlyPayRate() < 10 || myEmployee.getHourlyPayRate() > 75 || totalNumHours > 60);
			cout << endl << "Hourly pay rate successfully calculated";

			//.................................


			do {
				cout << endl << "Tax Rate Code: ";
				cin >> taxRate;
				if (taxRate != 1 && taxRate != 2 && taxRate != 3)
				{
					cout << endl << "Sorry, only tax rate codes between 1 and 3 are allowed.\nPlease try again" << endl;
				}
			} while (taxRate != 1 && taxRate != 2 && taxRate != 3);
			myEmployee.setTaxRate(taxRate);
			cout << endl << "Tax rate of " << myEmployee.getTaxRate() << "% selected successfully";


			//.................................


			do {
				cout << endl << "Work Status: ";
				cin >> workStatus;
				if (checkWorkStatus(workStatus) == false)
				{
					cout << endl << "Sorry, invalid work status. Only accepted values are F and P.\nPlease try again" << endl;
				}
			} while (checkWorkStatus(workStatus) == false);
			myEmployee.setWorkStatus(workStatus);
			if (myEmployee.getWorkStatus() == 'F') {
				cout << "Work status successfully updated to Full Time" << endl;
			}
			else {
				cout << "Work status successfully updated to Part Time" << endl;
			}
			//.................................


			AgencyEmployeePay agency;

			cout << "Company Name: ";
			cin >> companyName;
			agency.setCompanyName(companyName);
			cout << "Company name successfully set to " << agency.getCompanyName() << endl << endl;



			//---------------------------------Print Results------------------------

			output << "Hourly Employee Data and Calculations" << endl;

			myEmployee.printName();
			output << "Name: " << myEmployee.getFirstName() << " " << myEmployee.getLastName() << endl;

			myEmployee.printEmployeeId();
			output << "Employee Id: " << myEmployee.getEmployeeId() << endl;

			myEmployee.printSSN();
			output << "Social Security Number: " << myEmployee.getSSN() << endl;

			myEmployee.printTotalNumHours();
			output << "Number of hours worked in a week: " << myEmployee.getTotalNumHours() << endl;

			myEmployee.printWeeklyPay();
			output << "Weekly Pay: $" << myEmployee.getWeeklyPay() << endl;

			myEmployee.printOverTimePay();
			output << "Overtime pay: $" << myEmployee.getOverTimePay() << endl;

			myEmployee.printHourlyPayRate();
			output << "Hourly Pay Rate: $" << myEmployee.getHourlyPayRate() << endl;

			myEmployee.printTaxRate();
			output << "Tax Rate: " << myEmployee.getTaxRate() << "%" << endl;

			myEmployee.printWorkStatus();
			if (myEmployee.getWorkStatus() == 'F') {
				output << "Work Status: Full time" << endl;
			}
			else {
				output << "Work Status: Part time" << endl;
			}

			agency.printCompanyName();
			output << "Company Name: " << agency.getCompanyName() << endl;

			cout << endl;
			output << endl;
			//----------------------------------------------------------------------
		}
		else if (userInput == 1) {
			system("CLS");

			SalaryEmployeePay mySalEmp;


			cout << "Your full name: ";
			cin >> firstName;
			cin >> lastName;
			mySalEmp.setFirstName(firstName);
			mySalEmp.setLastName(lastName);


			do {
				cout << endl << "Social Security #: ";
				cin >> SSN;
				if (checkIfNum(SSN) == false)
				{
					cout << endl << "Sorry, Social security number is not in right format.\nTry again:";
				}
			} while (checkIfNum(SSN) == false);
			mySalEmp.setSSN(SSN);
			cout << endl << "SSN successfully taken";

			do {
				cout << endl << "Employee Id: ";
				cin >> employeeId;
				if (checkId(employeeId) == false)
				{
					cout << endl << "Sorry, Id is not in right format. Try again:";
				}
			} while (checkId(employeeId) == false);
			mySalEmp.setEmployeeId(employeeId);
			cout << endl << "Employee # successfully taken";


			do {
				cout << endl << "Annual pay:";
				cin >> annualPay;

				if (annualPay < 0) {
					cout << "Sorry, only positive values accepted.\nPlease try again." << endl;
				}
			} while (annualPay < 0);
			mySalEmp.setAnnualPay(annualPay);
			cout << endl << "Annual pay successfully taken";

			do {
				cout << endl << "Tax Rate Code: ";
				cin >> taxRate;
				if (taxRate != 1 && taxRate != 2 && taxRate != 3)
				{
					cout << endl << "Sorry, only tax rate codes between 1 and 3 are allowed.\nPlease try again" << endl;
				}
			} while (taxRate != 1 && taxRate != 2 && taxRate != 3);
			mySalEmp.setTaxRate(taxRate);
			cout << endl << "Tax rate of " << mySalEmp.getTaxRate() << "% selected successfully" << endl << endl;



			//---------------------------------Print Results------------------------
			output << "Salary Employee Data and Calculations" << endl;

			mySalEmp.printName();
			output << "Name: " << mySalEmp.getFirstName() << " " << mySalEmp.getLastName() << endl;

			mySalEmp.printEmployeeId();
			output << "Employee Id: " << mySalEmp.getEmployeeId() << endl;

			mySalEmp.printSSN();
			output << "Social Security Number: " << mySalEmp.getSSN() << endl;

			mySalEmp.printAnnualPay();
			output << "Annual Pay: $" << mySalEmp.getAnnualPay() << endl;

			mySalEmp.printTaxRate();
			if (mySalEmp.getTaxRate() == 1) {
				cout << "Tax Rate: 25%" << endl;
			}
			else if (mySalEmp.getTaxRate() == 2) {
				cout << "Tax Rate: 20%" << endl;
			}
			else {
				cout << "Tax Rate: 15%" << endl;
			}

			mySalEmp.printWeeklyPay();
			output << "Weekly Pay: $" << mySalEmp.getWeeklyPay() << endl;

			cout << endl;
			output << endl;
			//-----------------------------------------------------------------------
		}
	} while (userInput != 0);

	output.close();
	return 0;
}


bool checkWorkStatus(char status)
{

	bool result = true;
	status = toupper(status);
	if (status != 'F' && status != 'P')
	{
		result = false;
	}

	return result;
}

bool checkIfNum(string ssn)
{
	bool result = true;
	if (ssn.length() == 9) {
		for (int i = 0; i < ssn.length() && result == true; i++)
		{
			if (isdigit(ssn.at(i))) {
				result = true;
			}
			else {
				result = false;
			}
		}
	}
	else {
		result = false;
	}
	return result;
}

bool checkId(string employeeId) {
	bool result = true;
	if (employeeId.length() == 4) {
		for (int i = 0; i < employeeId.length() - 1 && result == true; i++)
		{
			if (isdigit(employeeId.at(i))) {
				result = true;
			}
			else {
				result = false;
			}
		}
		if (result && isalpha(employeeId.at(3)))
		{
			result = true;
		}
		else {
			result = false;
		}
	}
	else {
		result = false;
	}


	return result;
}




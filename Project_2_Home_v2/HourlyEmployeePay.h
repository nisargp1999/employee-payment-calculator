#ifndef HourlyEmployeePay_H
#define HourlyEmployeePay_H

#include "HourlyEmployee.h"

class HourlyEmployeePay : public HourlyEmployee
{
public:
	HourlyEmployeePay();
	double getOverTimePay();
	int getTaxRate();
	char getWorkStatus();
	void setWorkStatus(char &thisWorkStatus);
	void printOverTimePay();
	void printWorkStatus();
	void setTaxRate(char thisTaxRate);
	double getWeeklyPay();
	void printWeeklyPay();
	void printTaxRate();
private:
	void calcOverTimePay();
	void calcWeeklyPay();

	/*Employee thisEmployee;
	HourlyEmployee thisHourlyEmployeePay;*/
	double overTimePay;			//Calculated by function
	int taxRate;				//User input
	char workStatus;			//User input
	double weeklyPay;

};

#endif
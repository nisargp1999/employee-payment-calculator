#include "stdafx.h"
#include <string>
#include <iostream>
#include "AgencyEmployeePay.h"

AgencyEmployeePay::AgencyEmployeePay()
{
	companyName = "";
}

string AgencyEmployeePay::getCompanyName()
{
	return companyName;
}

void AgencyEmployeePay::setCompanyName(string thisCompanyName)
{
	companyName = thisCompanyName;
}

void AgencyEmployeePay::printCompanyName()
{
	cout << "Company Name: " << companyName << endl;
}

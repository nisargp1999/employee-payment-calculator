#ifndef AgencyEmployeePay_H
#define AgencyEmployeePay_H

#include "HourlyEmployee.h"
#include <string>
#include <iostream>

class AgencyEmployeePay : public HourlyEmployee
{
public:
	AgencyEmployeePay();
	string getCompanyName();
	void setCompanyName(string thisCompanyName);
	void printCompanyName();
private:
	/*Employee thisEmployee;*/
	string companyName;
};

#endif
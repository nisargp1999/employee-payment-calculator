#include "stdafx.h"
#include "Employee.h"

Employee::Employee()
{
	firstName = "";
	lastName = "";
	SSN = "";
	employeeId = "";
}

Employee::Employee(string thisFirstName, string thisLastName, string thisSSN, string thisEmployeeId)
{
	firstName = thisFirstName;
	lastName = thisLastName;
	SSN = thisSSN;
	employeeId = thisEmployeeId;
}

string Employee::getFirstName()
{
	return firstName;
}

void Employee::setFirstName(string thisFirstName)
{
	firstName = thisFirstName;
}

string Employee::getLastName()
{
	return lastName;
}

void Employee::setLastName(string thisLastName)
{
	lastName = thisLastName;
}

string Employee::getSSN()
{
	string result;
	result = "XXX-XX-" + SSN.substr(5, 4);
	SSN = result;
	return result;
}

string Employee::getEmployeeId()
{

	employeeId[3] = toupper(employeeId[3]);
	string result = employeeId.substr(0, 3) + "-" + employeeId.substr(3, 1);
	return result;
}

void Employee::printName()
{
	cout << "Name: " << firstName << " " << lastName << endl;
}

void Employee::printSSN()
{
	string result;
	result = "XXX-XX-" + SSN.substr(5, 4);
	cout << "Social Security Number: " << result << endl;
}

void Employee::printEmployeeId()
{
	employeeId[3] = toupper(employeeId[3]);
	string result = employeeId.substr(0, 3) + "-" + employeeId.substr(3, 1);
	cout << "Employee Id: " << result << endl;
}

void Employee::setSSN(string thisSSN)
{
	SSN = thisSSN;
}

void Employee::setEmployeeId(string thisEmployeeId)
{
	employeeId = thisEmployeeId;
}
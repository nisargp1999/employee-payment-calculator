#include "stdafx.h"
#include <string>
#include <iostream>
#include "HourlyEmployeePay.h"
#include "Employee.h"

HourlyEmployeePay::HourlyEmployeePay()
{
	taxRate = NULL;
	workStatus = NULL;
	overTimePay = NULL;
	weeklyPay = NULL;
}



void HourlyEmployeePay::calcWeeklyPay()
{
	if (taxRate == 1) {
		weeklyPay = (hourlyPayRate * totalNumHours) - (hourlyPayRate * totalNumHours * 0.25);
	}
	else if (taxRate == 2)
	{
		weeklyPay = (hourlyPayRate * totalNumHours) - (hourlyPayRate * totalNumHours * 0.2);
	}
	else {
		weeklyPay = (hourlyPayRate * totalNumHours) - (hourlyPayRate * totalNumHours * 0.15);
	}

}

double HourlyEmployeePay::getWeeklyPay()
{
	calcWeeklyPay();
	return weeklyPay;
}

void HourlyEmployeePay::printWeeklyPay()
{
	calcWeeklyPay();
	cout << "Weekly Pay: $" << weeklyPay << endl;
}

double HourlyEmployeePay::getOverTimePay()
{
	calcOverTimePay();
	return overTimePay;
}

int HourlyEmployeePay::getTaxRate()
{
	if (taxRate == 1) {
		return 25;
	}
	else if (taxRate == 2) {
		return 20;
	}
	else {
		return 15;
	}
}

char HourlyEmployeePay::getWorkStatus()
{
	toupper(workStatus);
	return workStatus;
}

void HourlyEmployeePay::setWorkStatus(char &thisWorkStatus)
{
	workStatus = toupper(thisWorkStatus);;
}

void HourlyEmployeePay::printOverTimePay()
{
	calcOverTimePay();
	cout << "Overtime pay: $" << overTimePay << endl;
}

void HourlyEmployeePay::printWorkStatus()
{
	if (workStatus == 'F') {
		cout << "Work Status: Full time" << endl;
	}
	else {
		cout << "Work Status: Part time" << endl;
	}
}

void HourlyEmployeePay::calcOverTimePay()
{

	overTimePay = 1.5 * getHourlyPayRate();
}

void HourlyEmployeePay::setTaxRate(char thisTaxRate)
{
	taxRate = thisTaxRate;
}

void HourlyEmployeePay::printTaxRate()
{
	if (taxRate == 1) {
		cout << "Tax Rate: 25%" << endl;
	}
	else if (taxRate == 2) {
		cout << "Tax Rate: 20%" << endl;
	}
	else {
		cout << "Tax Rate: 15%" << endl;
	}
}
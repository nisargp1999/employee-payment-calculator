#ifndef HourlyEmployee_H
#define HourlyEmployee_H

#include "Employee.h"

class HourlyEmployee : public Employee
{
public:
	HourlyEmployee();
	double getHourlyPayRate();
	double getTotalNumHours();
	void setTotalNumHours(double thisTotalNumHours);
	void printHourlyPayRate();
	void printTotalNumHours();
	double totalNumHours;	//user input
	double hourlyPayRate;	//input

	void setHourlyPayRate(double thisHourlyPayRate);
private:

	/*Employee thisEmployee;*/

};

#endif
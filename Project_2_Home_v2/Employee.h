#ifndef EMPLOYEE_H
#define EMPLOYEE_H



#include <string>
#include <iostream>

using namespace std;

class Employee
{
public:
	Employee();
	Employee(string thisFirstName, string thisLastName, string thisSSN, string thisEmployeeId);
	string getFirstName();
	void setFirstName(string thisFirstName);
	string getLastName();
	void setLastName(string thisLastName);
	string getSSN();
	string getEmployeeId();
	void printSSN();
	void printEmployeeId();
	void setSSN(string thisSSN);
	void setEmployeeId(string thisEmployeeId);
	void printName();
private:
	string firstName;
	string lastName;
	string SSN;
	string employeeId;
};

#endif // !EMPLOYEE_H


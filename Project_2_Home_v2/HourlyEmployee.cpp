#include "stdafx.h"
#include <string>
#include <iostream>
#include "HourlyEmployee.h"

HourlyEmployee::HourlyEmployee()
{
	totalNumHours = NULL;
	hourlyPayRate = NULL;
}

double HourlyEmployee::getHourlyPayRate()
{
	return hourlyPayRate;
}

double HourlyEmployee::getTotalNumHours()
{
	return totalNumHours;
}

void HourlyEmployee::setTotalNumHours(double thisTotalNumHours)
{
	totalNumHours = thisTotalNumHours;

}



void HourlyEmployee::setHourlyPayRate(double thisHourlyPayRate)
{
	hourlyPayRate = thisHourlyPayRate;

}

void HourlyEmployee::printHourlyPayRate()
{
	cout << "Hourly Pay: $" << hourlyPayRate << endl;
}

void HourlyEmployee::printTotalNumHours()
{
	cout << "Number of hours worked in a week: " << totalNumHours << endl;
}


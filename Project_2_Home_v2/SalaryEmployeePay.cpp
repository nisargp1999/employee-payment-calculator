#include "stdafx.h"
#include <string>
#include <iostream>
#include "SalaryEmployeePay.h"
using namespace std;

SalaryEmployeePay::SalaryEmployeePay()
{
	annualPay = NULL;
	taxRate = NULL;
}

void SalaryEmployeePay::setAnnualPay(double thisAnnualPay)
{
	annualPay = thisAnnualPay;
}

void SalaryEmployeePay::setTaxRate(int thisTaxRate)
{
	taxRate = thisTaxRate;
}

void SalaryEmployeePay::calcWeeklyPay()
{
	if (taxRate == 1) {
		weeklyPay = (annualPay / 52.142) - ((annualPay / 52.142) * 0.25);
	}
	else if (taxRate == 2)
	{
		weeklyPay = (annualPay / 52.142) - ((annualPay / 52.142) * 0.2);
	}
	else {
		weeklyPay = (annualPay / 52.142) - ((annualPay / 52.142) * 0.15);
	}
}

double SalaryEmployeePay::getAnnualPay()
{
	return annualPay;
}

double SalaryEmployeePay::getWeeklyPay()
{
	calcWeeklyPay();
	return weeklyPay;
}

int SalaryEmployeePay::getTaxRate()
{
	if (taxRate == 1) {
		return 25;
	}
	else if (taxRate == 2) {
		return 20;
	}
	else {
		return 15;
	}
}

void SalaryEmployeePay::printAnnualPay()
{
	cout << "Annual Pay: $" << annualPay << endl;
}

void SalaryEmployeePay::printWeeklyPay()
{
	calcWeeklyPay();
	cout << "Weekly Pay: $" << weeklyPay << endl;
}

void SalaryEmployeePay::printTaxRate()
{
	if (taxRate == 1) {
		cout << "Tax Rate: 25%" << endl;
	}
	else if (taxRate == 2) {
		cout << "Tax Rate: 20%" << endl;
	}
	else {
		cout << "Tax Rate: 15%" << endl;
	}
}

